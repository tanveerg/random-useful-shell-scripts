# pre requisite is to install fzf


export FZF_DEFAULT_OPTS='--height 40% --reverse --border'

# function to quickly cd into a dirrectory
cds(){
cd ~
dir=$(find ~/ -type d | fzf)
if [ -z "$dir" ]; then
    echo "nothing chosen"
    cd -
else
    cd $dir
fi
}
