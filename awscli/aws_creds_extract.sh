CREDS_JSON=$(curl -qL ${IP}${AWS_CONTAINER_CREDENTIALS_RELATIVE_URI})
export AWS_ACCESS_KEY_ID=$(echo $CREDS_JSON | jq -r '.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo $CREDS_JSON | jq -r '.SecretAccessKey')
export AWS_SESSION_TOKEN=$(echo $CREDS_JSON | jq -r '.Token')