# cds utility with the fish shell, the syntax is slightly different than the 
# traditional bash syntax

# another pre-requisite to have is to have this in your fish_variables
SETUVAR --export FZF_DEFAULT_OPTS:--height 40% --reverse --border

# this is the main cds function
function cds
	set -l dir (find ~/ -type d | fzf)
	if [ -z "$dir" ]
		echo "nothing chosen"
		cd -
	else
		cd $dir
	end
end