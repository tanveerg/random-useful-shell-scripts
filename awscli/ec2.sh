
# account id
ACCOUNT_ID=$(aws sts get-caller-identity  --output text --query 'Account')

# get latest ec2 image query
ECS_OPTIMIZED_AMI=$(aws ec2 describe-images --owners ${ACCOUNT_ID} --filters 'Name=description,Values=AWS ECS Optimized as Base Ami' --query 'Images[*].[ImageId, CreationDate] | reverse(sort_by(@, &[1])) | [0][0]' --output text)
