

# account name
ACCOUNT_NAME=$(aws iam list-account-aliases --output text --query 'AccountAliases')

# account id
ACCOUNT_ID=$(aws sts get-caller-identity  --output text --query 'Account')
