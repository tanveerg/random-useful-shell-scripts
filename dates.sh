#!/bin/bash
startdate=$1
enddate=$2

dates=()
for (( date="$startdate"; date != enddate; )); do
    dates+=( "$date" )
    date="$(date --date="$date + 1 days" +'%Y%m%d')"
    echo $date
done

# prints dates between ranges