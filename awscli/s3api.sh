#!/bin/bash
set -e

S3_BUCKET=$1

SUB_FOLDER=$2

LATEST_FILE_IN_LOCATION=$(aws s3api list-objects-v2 --bucket ${S3_BUCKET} --prefix ${SUB_FOLDER} --query 'Contents[*].[Key, LastModified] | reverse(sort_by(@, &[1])) | [0][0]' --output text)
