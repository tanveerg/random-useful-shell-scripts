 function kms_decrypt() {
  aws kms decrypt --ciphertext-blob fileb://<(echo $1 | base64 -D) --output text --query Plaintext | base64 -D
}
