#!/usr/bin/env groovy

pipeline {
  agent none

  options {
    timestamps()
    disableConcurrentBuilds()
  }

  environment {
    AWS_REGION='eu-west-1'
    AWS_DEFAULT_REGION='eu-west-1'
    PYTHONUNBUFFERED=1
    MODEL_ALGO='treelite'
    MODEL_VERSION='1'
    TRAINING_DATE='20190703'
  }

  stages {

    stage('Build a new docker image and push it to ECR if it doesn\'t exist.') {
      when { not {branch 'lightgbm-model'} }
      agent { label 'build-docker' }
      steps {
        dir('api') {
          script {
              dockerTag = env.GIT_COMMIT.substring(0, 7)
              modelAlgo = env.MODEL_ALGO
              trainingDate = env.TRAINING_DATE
              modelVersion = env.MODEL_VERSION
              echo "docker image tag - ${dockerTag}"
              sh "./tag_and_push_image.sh ${dockerTag} ${modelAlgo} ${trainingDate} ${modelVersion}"
          }
        }
      }
    }

    stage('Deploy CloudFormation Stack - Dev') {
      when { not {branch 'lightgbm-model'} }
      agent { label 'deploy-docker-dev' }
      steps {
          script {
            echo "docker image tag - ${dockerTag}"
            dir('deployment') {
              sh "./deploy.sh dev t2.xlarge ${dockerTag} ${modelAlgo} ${trainingDate} ${modelVersion} 1024 2048 2"
           }
        }
      }
    }

    stage('Integration Test - Dev') {
      when { not {branch 'lightgbm-model'} }
      agent { label 'dpe-build-python36' }
      steps {
        dir('api') {
            sh "./integration_tests.sh"
            sh "python model_testing.py "
        }
      }
    }

    stage('Push docker image with the latest tag to ECR') {
      when {branch 'lightgbm-model'}
      agent { label 'build-docker' }
      steps {
            dir('api') {
              script{
                modelAlgo = env.MODEL_ALGO
                trainingDate = env.TRAINING_DATE
                modelVersion = env.MODEL_VERSION
                sh "./tag_and_push_image.sh latest ${modelAlgo} ${trainingDate} ${modelVersion}"
              }
            }
        }
    }

    stage('Deploy CloudFormation Stack - Prod') {
      when {branch 'lightgbm-model'}
      agent { label 'deploy-docker-prod' }
      steps {
          script {
            dir('deployment') {
              sh "./deploy.sh prod c5.4xlarge latest ${modelAlgo} ${trainingDate} ${modelVersion} 1024 2048 2"
            }
          }
        }
     }

    stage('Integration Test - Prod') {
      when {branch 'lightgbm-model'}
      agent { label '' }
      steps {
        dir('api') {
            sh "./integration_tests.sh "
            sh "python model_testing.py 
        }
      }
    }
  }
  post{
    success{
        slackSend channel: '',
             color: 'good',
             message: "Project build and deployment successful - ${env.BUILD_URL}. Job details - ${env.JOB_NAME} [${env.BUILD_NUMBER}]"
        }
    failure{
         slackSend channel: '',
              color: 'bad',
              message: "Failed build - ${env.BUILD_URL}. Job details - ${env.JOB_NAME} [${env.BUILD_NUMBER}]"
     }
   }
}
